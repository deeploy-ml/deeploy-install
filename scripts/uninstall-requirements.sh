#!/bin/bash

# Versions
export ISTIO_VERSION=1.21.6
export KNATIVE_VERSION=knative-v1.13.1
export KNATIVE_NET_ISTIO_VERSION=knative-v1.13.1
export CERT_MANAGER_VERSION=v1.13.6

helm uninstall istio-ingressgateway -n istio-system
helm uninstall istiod -n istio-system
helm uninstall istio-base -n istio-system

kubectl delete ns istio-system

# Knative
kubectl delete -f https://github.com/knative/serving/releases/download/${KNATIVE_VERSION}/serving-crds.yaml
kubectl delete -f https://github.com/knative/serving/releases/download/${KNATIVE_VERSION}/serving-core.yaml
kubectl delete -f https://github.com/knative/net-istio/releases/download/${KNATIVE_NET_ISTIO_VERSION}/net-istio.yaml

# Cert-manager
kubectl delete -f https://github.com/jetstack/cert-manager/releases/download/${CERT_MANAGER_VERSION}/cert-manager.yaml
